<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>....</title>
		<link type="text/css" rel="stylesheet" href="../../assets/css/style.min.css"/>
	</head>
	<body>
		<div class="admincontenedor">
			<h2>Subir Promo</h2>
			<form action="upload.php" method="post" enctype="multipart/form-data">
			    Seleccionar una imagen para subir:
				<input type="file" name="fileToUpload" id="fileToUpload">
				<input class="btn" type="submit" value="Subir promo" name="submit">
			</form>
		</div>
		<hr>
		<h2 class="eliminarpromo">Eliminar Promos</h2>
		<div class="contenedorpromos limpiar">
			<?php
				$all_files = glob("../promo/*.*");
				for ($i=0; $i<count($all_files); $i++){
					$image_name = $all_files[$i];
					$supported_format = array('gif','jpg','jpeg','png');
					$ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
					if (in_array($ext, $supported_format)){
						echo '
			<div class="itemadmin">
				<img src="'.$image_name .'" alt="'.$image_name.'" />'.'
				<a href="eliminar.php?file='.$image_name.'">Eliminar</a>
			</div>';
					} else {
						continue;
					}
				}
			?>
		</div>
	</body>
</html>