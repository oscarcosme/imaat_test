<?php
	function getRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    define('UPLOAD_FOLDER', "../promo/");

	if (!isset($_FILES['fileToUpload'])) {
        return '';
    }else{
    	$imgs = '';

        $files = $_FILES['fileToUpload'];

        if ($files['error'] === 0) {
            $ext = explode(".", strtolower($files['name']));
            //if($ext[(count($ext)-1)] == 'pdf'){
                $name = getRandomString(20).'.'.$ext[(count($ext)-1)];

                if (move_uploaded_file($files['tmp_name'], UPLOAD_FOLDER.$name) === true) {
                    $imgs = $name;
                }
            //}
        }
        echo $imgs;
    }
    header('Location: ./');
?>