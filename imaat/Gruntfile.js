module.exports = function(grunt) {
	grunt.registerTask('build', [ 'clean:build', 'jshint', 'concat:jsbuild', 'uglify:js', 'sass:stylebuild', 'autoprefixer:build', 'copy:buildhtml', 'copy:buildphp', 'copy:buildimages', 'copy:buildfonts', 'copy:buildlibs', 'copy:buildextras', 'copy:buildfixed', 'notify:build' ]);
	grunt.registerTask('debug', [ 'clean:debug', 'jshint', 'concat:jsdebug', 'sass:styledebug', 'autoprefixer:debug', 'copy:debughtml', 'copy:debugphp', 'copy:debugimages', 'copy:debugfonts', 'copy:debuglibs', 'copy:debugextras', 'copy:debugfixed', 'notify:debug' ]);
	grunt.registerTask('inicio', ['php', 'browserSync', 'watch']);

	grunt.initConfig({

		php: {
			test: {
				options: {
					base: 'debug',
					port: 8010,
				}
			},
		},
		browserSync: {
			dev: {
				bsFiles: {
					src: 'debug/**',
				},
				options: {
					proxy: '127.0.0.1:8010',
					port: 8080,
					open: true,
					watchTask: true,
				}
			},
		},

		//VALIDAR JS
		jshint: { src: 'source/assets/js/*.js' },

		//CONCATENA LOS JS, LOS MINIFICA Y OFUSCA
		concat: {
			jsbuild: {
				options: { separator: ';' },
				src: 'source/assets/js/*.js',
				dest: 'build/assets/js/script.min.js',
			},
			jsdebug: {
				options: { separator: ';' },
				src: 'source/assets/js/*.js',
				dest: 'debug/assets/js/script.min.js',
			},
		},

		//
		uglify: {
			js: {
				options: { mangle: true },
				src: 'build/assets/js/script.min.js',
				dest: 'build/assets/js/script.min.js',
			},
		},

		// COMPILA SASS, LE AÑADE PREFIJOS Y LO MINIFICA
		sass: {
			stylebuild: {
				options:{ style:'compressed' },
				src: 'source/assets/css/style.scss',
				dest: 'build/assets/css/style.min.css',
			},
			styledebug: {
				options:{},
				src: 'source/assets/css/style.scss',
				dest: 'debug/assets/css/style.min.css',
			},
		},

		//
		autoprefixer: {
			build: {
				src: 'build/assets/css/style.min.css',
				dest: 'build/assets/css/style.min.css',
			},
			debug: {
				src: 'debug/assets/css/style.min.css',
				dest: 'debug/assets/css/style.min.css',
			},
		},

		//
		copy: {
			buildhtml: {
				expand: true,
				cwd: 'source',
				src: ['*.html'],
				dest: 'build',
			},      
			buildphp: {
				expand: true,
				cwd: 'source',
				src: ['*.php'],
				dest: 'build',
			},
			buildimages: {
				expand: true,
				cwd: 'source/assets/img',
				src: '**/*',
				dest: 'build/assets/img',
			},
			buildfonts: {
				excludeEmpty: false,
				expand: true,
				cwd: 'source/assets/css/fonts',
				src: '*.{eot,svg,ttf,woff,woff2}',
				dest: 'build/assets/css/fonts',
			},
			buildlibs: {
				expand: true,
				cwd: 'source/libs',
				src: '**/*',
				dest: 'build/libs',
			},
			buildextras: {
				expand: true,
				cwd: 'source/extras',
				src: '**/*',
				dest: 'build/extras',
			},
			buildfixed: {
				expand: true,
				cwd: 'source/fixed',
				src: '**/*.php',
				dest: 'build/fixed',
			},
			
			debughtml: {
				expand: true,
				cwd: 'source',
				src: ['**/*.html'],
				dest: 'debug'
			},
			debugphp: {
				expand: true,
				cwd: 'source',
				src: ['*.php'],
				dest: 'debug',
			},
			debugimages: {
				expand: true,
				cwd: 'source/assets/img',
				src: '**/*',
				dest: 'debug/assets/img',
			},
			debugfonts: {
				excludeEmpty: false,
				expand: true,
				cwd: 'source/assets/css/fonts',
				src: '*.{eot,svg,ttf,woff,woff2}',
				dest: 'debug/assets/css/fonts',
			},
			debuglibs: {
				expand: true,
				cwd: 'source/libs',
				src: '**/*',
				dest: 'debug/libs',
			},
			debugextras: {
				expand: true,
				cwd: 'source/extras',
				src: '**/*',
				dest: 'debug/extras',
			},
			debugfixed: {
				expand: true,
				cwd: 'source/fixed',
				src: '**/*.php',
				dest: 'debug/fixed',
			},
		},
		// LIMPIA CARPETA BUILD
		clean: {
			build: { src: [ 'build' ] },
			debug: { src: [ 'debug' ] },
		},

		// VIGILA CAMBIOS 
		watch: {
			options: { livereload:false },
			js: {
				files: ['source/assets/js/*.js'],
				tasks: [ 'jshint', 'concat:jsdebug', 'notify:watch'],
			},
			jsLibs: {
				files: ['source/libs/**/*'],
				tasks: ['copy:debuglibs', 'notify:watch'],
			},
			css: {
				files: ['source/assets/css/*.scss'],
				tasks: ['sass:styledebug', 'autoprefixer:debug', 'notify:watch'],
			},
			html: {
				files: ['source/**/*.html'],
				tasks: ['copy:debughtml', 'notify:watch'],
			},         
			php: {
				files: ['source/*.php'],
				tasks: ['copy:debugphp', 'notify:watch'],
			},      
			image: {
				files: ['source/assets/img/*.{png,jpg,jpeg,gif,svg}'],
				tasks: ['copy:debugimages', 'notify:watch'],
			},
			fonts: {
				files: ['source/assets/css/font/*.{eot,svg,ttf,woff,woff2}'],
				tasks: ['copy:debugfonts', 'notify:watch'],
			}, 
			extras: {
				files: ['source/extras/**/*'],
				tasks: ['copy:debugextras', 'notify:watch'],
			},
			fixed: {
				files: ['source/fixed/**/*'],
				tasks: ['copy:debugfixed', 'notify:watch'],        
			},
		},

		notify_hooks: {
			options: {
				enabled: true,
				max_jshint_notifications: 5,
				success: false, 
				duration: 5,
			},
		},
		notify: {
			build: {
				options: {
					message: 'Build completado!',
				},
			},
			debug: {
				options: {
					message: 'Debug completado!',
				},
			},
			watch: {
				options: {
					message: 'Watch completado!',
				},
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-php');
};